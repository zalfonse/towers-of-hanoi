TITLE ;Name of program, author, date

INCLUDE C:/Irvine/Irvine32.inc
.data ; data segment

	
	coordx db ?
	coordy db ?
	colorcode db ?

.code ; code segment
	main proc ; main procedure
	call Randomize

	mov ecx, 1000

	makeCoords:
		mov eax, 25
		Call RandomRange
		mov coordx, al

		mov dh, al

		mov eax, 80
		Call RandomRange
		mov coordy, al

		mov dl, al
		
		mov eax, 16
		Call RandomRange
		mov colorcode, al

		;Call Clrscr
		Call Gotoxy
				
		mov al, colorcode
		Call SetTextColor
		mov al, 24h
		Call WriteChar

		mov eax, 250
		call Delay
	Loop makeCoords

	exit ; end program
main endp ; end procedure
end main ; end program	