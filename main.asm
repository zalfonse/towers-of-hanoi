INCLUDE C:/Irvine/Irvine32.inc
.data
	;Messages that are displayed at various points in the game
	welcome1 byte "Welcome to Tower of Hanoi",0
	welcome2 byte "Enter how many discs you would like to play with: ",0
	win byte "You have won! Score: ",0
	badMove byte "Illegal Move! The disk is Larger than the slot your trying to place it in.",0

	;Character codes for the various painting elements for clarity's sake
	pole dd 179
	disc dd 220
	arrow dd 206 


	; Keeps final game score and number of moves taken
	moves dw 0
	score dw 0

	; Game time in milliseconds, gameTime is actually a representation of how many 10's of seconds 
	gameStart dd ?
	gameEnd   dd ?
	gameTime dd ?

	; Various Variables to keep track of where we are in the game
	numOfDiscs byte 0 ; Total number of discs
	tempDisc   byte ? ; A disc that is not tied to a tower for referencing (mostly used by peek)
	currentDisc db ? ; The disc that we have selected
	currentSpot db 0 ; The tower we are over

	; The three Towers of Hanoi
	leftstack byte 10 dup(0)
	middlestack byte 10 dup(0)
	rightstack byte 10 dup(0)

	; Various game states
	gameOver byte 0

	spare BYTE 1 ; spare, start, dest are the pole labels, 1 is far left 2 is middle 3 is far right
	start BYTE 0
	dest BYTE 2
	disk BYTE 3 ; (total # of disks) - 1 also used as a reference to the disk, 0 being your largest disk 5 being your smallest


.code 
	main proc
		call WelcomeToGame ; Splash the start
		mov al, numOfDiscs
		mov disk, al
		dec disk
		call GetMSeconds ; Get the start time of the game
		mov gameStart, eax ; save it
		call drawGame ; draw the start of the game
		call gameLoop ; lets go
		exit
	main endp
	
	; Main game loop
	gameLoop proc uses ecx
		mov ecx, 1
		top:
			call getInput ; Wait for input, when recieved act accordinly
			call drawGame ; Draw whatever state the game is in
			call checkWin ; Check if we've won
			inc ecx	; increment ECX to create infinite loop
			cmp gameOver, 0 ; if the game is over
			jnz over	 ; leave the loop
		loop top
		over: ; What to do when the game ends
			mov dl,0
			mov dh,0
			call gotoXY ; Move the cursor so the message prints in the right place
		ret
	gameLoop endp

	calcScore proc uses eax ecx edx 
	; Score is calculated based on 3 elements
	; - How long it took you
	; - How many discs were in your game
	; - How many moves you took
		mov edx, 0
		mov ecx, 5000
		mov ax, 1000

		sub ax, moves ; you lose 1 point per move taken
		mov score, ax 
		mov eax, gameEnd
		sub eax, gameStart
		mov gameTime, eax
		div ecx
		
		sub score, ax ; you lose 1 point per 10 seconds
		

		movzx ecx, numOfDiscs
		sizepoints:
			add score, 350 ; 350 base points per disc in the game
		loop sizepoints
		ret
	calcScore endp

	checkWin proc uses eax edx
	; Checks to see if you've won, if you have print win and the score
		mov edi, offset rightstack
		movzx ebx, numOfDiscs
		dec ebx
		mov eax, [edi + ebx]
		.if eax == 1 ; If the highest point on the rightmost tower is 1
			call getMSeconds
			mov gameEnd, eax
			mov dl, 24
			mov dh, 4
			call gotoXY
			mov edx, offset win
			call WriteString ; print the message
			call CalcScore
			movzx eax, score
			call WriteInt ; prints the score
			mov gameOver, 1 ; end the game
		.endif	 
		ret
	checkWin endp

	getInput proc uses eax ecx edx edi
		call ReadChar ; gets input for char from user

		.if al == 'q'
			call take
		.endif

		.if al =='e' ; only action that counds as a move *** Fix this so that an illegal place doesn't count as a move ***
			call place
			inc moves
		.endif

		.if al == 'a'
			call moveLeft
		.endif

		.if al == 'd'
			call moveRight
		.endif

		.if al == 'y'
			call recursiveSolution
		.endif

		ret
	getInput ENDP

	moveLeft proc ; Moves the arrow to the left to select the stack to the left, wont let you go off the 3 stacks
		.if currentSpot > 0
			dec currentSpot
		.endif
		ret
	moveLeft endp

	moveRight proc; Moves the arrow to the right to select the stack to the right, wont let you go off the 3 stacks
		.if currentSpot < 2
			inc currentSpot
		.endif
		ret
	moveRight endp

	drawGame proc uses eax ebx ecx edx esi edi ; Draws the entire game in its currents state
		call clrscr
		.if currentDisc < 10
			.if currentDisc > 0 ; if we have a disc, draw the disc in the right spot
				movzx ecx, currentDisc
				.if currentSpot == 0
					mov dl, 10
					mov dh, 5
				.elseif currentSpot == 1
					mov dl, 35
					mov dh, 5
				.elseif currentSpot == 2
					mov dl, 60
					mov dh, 5
				.endif
				call drawDisc
			.elseif currentDisc == 0 ; if we dont have a disc, draw the arrow in the right spot
				.if currentSpot == 0
					mov dl, 10
					mov dh, 5
				.elseif currentSpot == 1
					mov dl, 35
					mov dh, 5
				.elseif currentSpot == 2
					mov dl, 60
					mov dh, 5
				.endif
				call drawArrow
			.endif
		.endif

		;Draw the Left Tower
		mov edi, offset leftstack
		mov dl, 10 ; x
		mov dh, 23 ; y
		call drawTower

		;Draw the Middle Tower
		mov edi, offset middlestack
		mov dl, 35 ; x
		mov dh, 23 ; y
		call drawTower

		;Draw the Right Tower
		mov edi, offset rightstack
		mov dl, 60 ; x
		mov dh, 23 ; y
		call drawTower

		ret
	drawGame endp


	drawTower proc uses ecx ; This function draws the three towers
		;dl is the x starting coordinate for the bottom center of the tower
		;dh is the y startinc coordinate for the bottom center of the tower
		;move the base address of the tower you want to draw into edi

		call drawPole
		mov ecx, 10

		tower:
			mov al, 10
			movzx eax, al
			sub eax, ecx
			mov bl, [edi+eax]
			cmp bl, 0
			jz skip 
			
			push ecx
				movzx ecx, bl
				call drawDisc
				call CRLF
			pop ecx
			
			skip:
				dec dh
		loop tower
			
		mov dl, 0
		mov dh, 0
		call gotoXY
		
		ret
	drawTower endp

	drawDisc proc uses edx ecx eax ; This function draws a disc with its center at given location dl (x), dh (y), with given size ecx
		mov eax, disc
		mov bl, dl ;set our anchor x

		draw:
			add dl, cl
			Call gotoXY
			Call WriteChar
			mov dl, bl

			sub dl, cl
			Call gotoXY
			Call WriteChar
			mov dl, bl
		loop draw
		
		ret
	drawDisc endp

	drawPole proc uses edx ; draws a pole that has a bottom location at dl,dh
		mov eax, pole
		mov ecx, 10
		l1:
			call gotoXY
			call WriteChar
			dec dh
		loop l1
		ret
	drawPole endp

	drawArrow proc ; writes the selection arrow to the screen at dl,dh
		call gotoXY
		mov eax, arrow
		call WriteChar
		ret
	drawArrow endp

	take proc ; This function takes the current disc off of the selected stack using eax to select
		movzx eax, currentSpot
		.if currentDisc != 0 ; Not allowed to take if you already have a disc
			ret
		.endif		
		.if eax == 0 ; Take the top disc off the left stack and move it to the currentDisk
			mov edi, offset leftstack
			call myPop
		.elseif eax == 1 ; Move the current selected disc onto the middle stack
			mov edi, offset middlestack
			call myPop
		.elseif eax == 2 ; Move the current selected disc onto the right stack
			mov edi, offset rightstack
			call myPop
		.endif
		ret
	take endp

	place proc uses edx eax edi ; This function places the current disc onto the selected stack using eax to select
		movzx eax, currentSpot
		.if currentDisc == 0 ; Not allowed to place if you have no disc (even though it would do nothing, just for sanity)
			ret
		.endif
		.if eax == 0 ; Move the current selected disc onto the left stack
			mov edi, offset leftstack
			call myPeek
			mov bl, tempDisc
			.if currentDisc < bl
				call myPush
			.else
				; **** ERROR, ILLEGAL MOVE ****
			.endif
			.if bl == 0
				call myPush
			.endif
		.elseif eax == 1 ; Move the current selected disc onto the middle stack
			mov edi, offset middlestack
			call myPeek
			mov bl, tempDisc
			.if currentDisc < bl
				call myPush
			.else
				; **** ERROR, ILLEGAL MOVE ****
			.endif
			.if bl == 0
				call myPush
			.endif
		.elseif eax == 2 ; Move the current selected disc onto the right stack
			mov edi, offset rightstack
			call myPeek
			mov bl, tempDisc
			.if currentDisc < bl
				call myPush
			.else
				; **** ERROR, ILLEGAL MOVE ****
			.endif
			.if bl == 0
				call myPush
			.endif
		.endif
		ret
	place endp

	myPop proc uses edi ; takes the first non-zero value from the right and puts it into currentDisc, the array that it uses is in edi 
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1

			mov bl, [edi]
			movzx eax, bl
			call WriteInt
			call CRLF
		
		L2:
			add edi, ecx
			mov bl, 0
			mov [edi], bl
			mov currentDisc, al
		ret
	myPop endp

	myPush proc uses edi ; puts the value of currentDisc into the last zero value from the right, the array it uses is in edi
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1

		L2:
			cmp ecx, 0
			jz skip
			inc edi
			skip:
			mov al, [edi]
			.if al != 0
				.if ecx == 0
					inc edi
				.endif
			.endif
			add edi, ecx
			movzx edx, currentDisc
			mov [edi], edx
			mov currentDisc, 00
		ret
	myPush endp 

	myPeek proc uses edi ; puts the value of the first non-zero value in the array held in edi into tempDisc
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1
			mov bl, [edi + ecx]
			movzx eax, bl
		L2:
			mov tempDisc, al
		ret
	myPeek endp
	
	printTower proc ;prints tower located at edi
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			call WriteInt
			call CRLF
		loop L1
			mov bl, [edi]
			movzx eax, bl
			call WriteInt
			call CRLF
		ret
	printTower endp

	recursiveSolution PROC
		;inc ebx ; meaningless - ignore
		.if disk == 0 ; Base case - If the you have reached the 0th disk aka the largest disk in the particular tower then move it on over
			mov al, start
			mov currentSpot, al
			call Take
			call DrawGame
			mov eax, 100
			call Delay
			mov al, dest
			mov currentSpot, al
			call Place
		.else ; If you get here then you cannot move your biggest disk
			dec disk ; dec disk so that the recursiveSolution is now moving over a tower of size disk - 1
			call swap1 ; This swaps the destination pole to the spare pole and vice versa.
			call recursiveSolution ; move the tower of disk-1 disks to your spare pole to get them out of the way!
			call swap1 ; Now you have to swap back your spare and destination for the sake of sanity
			inc disk ; inc disk back to its original value because global variables, assembly etc

			mov al, start ; now that the tower sitting on top of your largest disk is gone, you van go ahead and move that thing over to your original destination
			mov currentSpot, al
			call Take
			mov al, dest
			mov currentSpot, al
			call Place
			; Here comes the tricky part! aka do it all over again forever (not really)
			call swap2 ; Swap your source with your spare because thats where your remaining tower is now sitting
			dec disk ; once again, we need to move the tower of disk-1 disks
			call recursiveSolution ; move the tower of disk-1 disks to your destination from your spare (except now its labeled as source and vice versa)
			inc disk ; change back
			call swap2 ; change back
		.endif
			call DrawGame
			mov eax, 100
			call Delay
		ret
	recursiveSolution ENDP

	swap1 PROC uses eax ; swaps destination and spare pole

		mov al, spare
		xchg dest, al
		xchg spare, al
	ret
	swap1 ENDP

	swap2 PROC uses eax ; swaps source pole and destination pole
		mov al, spare 
		xchg start, al
		xchg spare, al
	ret
	swap2 ENDP

	WelcomeToGame proc ; Splash screen
		mov edx,offset welcome1
		call writestring
		call crlf
		mov edx,offset welcome2
		call writestring
		call readint
		mov numOfDiscs,al

		mov ecx, 0	
		mov esi, 0
		mov cl, numOfDiscs
		mov esi, offset leftstack

		Looper:
			mov [esi], cl
			inc esi
		Loop Looper
		

		ret
	WelcomeToGame endp

end main 